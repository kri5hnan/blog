class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates :title, :body, presence: true
  validates_associated :comments 
end
